INTRODUCTION
------------

The goal of the project is to integrate a Drupal CMS with Experian Marketing Services Technology (EMST) platform � CheetahMail France.

FEATURES
--------
�
This first version of the module allows customers to integrate Drupal with Cheetahmail for subscribing, unsubscribing and sending NewsLetters. The features are:
�
- Synchronizing subscribing/unsubscribing contacts to CheetahMail
- Sending automatic email and Newsletters.
